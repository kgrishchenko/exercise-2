# exercise-2



## Процесс запуска (Ubuntu 22.04)

* Install docker

```
apt update
apt install docker.io
```
* Install Docker Compose
```
apt -y install docker-compose
```
* Clone repository
```
git clone git@gitlab.com:simbirsoft1/devops-workshop-2023-04/konstantin.grishchenko/exercise-2.git
```
* Run Docker-compose
```
docker-compose up -d
```

* Testing

```
curl -i -X POST -d '{ "username": "user123", "email": "user@example.com", "password_hash": "example" }' -H "Content-Type: application/json" http://127.0.0.1:5000/api/user
```


***

# Разбор в ручном запуске 

* postgres

```
createdb app -O worker
psql -d app
app=> \password
Enter new password for user "worker":
```

* Python-3.6.15

```
git clone https://github.com/mbaran0v/python-sample-app
cd python-sample-app
python3.6 -m venv env36
source env36/bin/activate
sudo apt-get install libpq-dev #нужен для установки psycopg2==2.8.3 из requirements.txt
pip install -r requirements.txt
export FLASK_APP=app.py
export POSTGRESQL_URL=postgresql://worker:worker@localhost/app
flask db upgrade
python3 app.py
curl -i -X POST -d '{ "username": "user123", "email": "user@example.com", "password_hash": "example" }' -H "Content-Type: application/json" http://127.0.0.1:5000/api/user

pip install wheel
pip install uwsgi
uwsgi --socket 0.0.0.0:5000 --protocol=http -w app:api

pip install gunicorn
gunicorn --bind 0.0.0.0:5000 app:api
```
