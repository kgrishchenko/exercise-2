FROM python:3.6.15-slim

WORKDIR /app

COPY . /app

RUN apt-get update \
&& apt-get install --assume-yes libpq-dev gcc \
&& apt-get clean \
&& pip install --no-cache gunicorn \
&& pip install --no-cache -r requirements.txt \
&& apt-get purge --assume-yes gcc \
&& apt-get autoremove --assume-yes \
&& rm -rf /var/lib/apt/lists/*

EXPOSE 5000

ENTRYPOINT [ "/bin/sh", "/app/entrypoint.sh" ]

